/******************************************************************************
 * Copyright (c) 2017, Texas Instruments Incorporated - http://www.ti.com
 *   All rights reserved.
 *
 *   Redistribution and use in source and binary forms, with or without
 *   modification, are permitted provided that the following conditions are met:
 *       * Redistributions of source code must retain the above copyright
 *         notice, this list of conditions and the following disclaimer.
 *       * Redistributions in binary form must reproduce the above copyright
 *         notice, this list of conditions and the following disclaimer in the
 *         documentation and/or other materials provided with the distribution.
 *       * Neither the name of Texas Instruments Incorporated nor the
 *         names of its contributors may be used to endorse or promote products
 *         derived from this software without specific prior written permission.
 *
 *   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 *   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 *   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 *   ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 *   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 *   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 *   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 *   THE POSSIBILITY OF SUCH DAMAGE.
 *****************************************************************************/

/* This file contains lib_emt_xxx functions based on EDMA3 LLD for K2G only */
#ifdef SOC_K2G      

#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#include <xdc/std.h>
#include <ti/csl/csl_tsc.h>
#include <ti/sdo/edma3/drv/edma3_drv.h>
#include <ti/sdo/edma3/drv/sample/bios6_edma3_drv_sample.h>
#include "lib_datatrans.h"

#define LIB_EMT_EDMA3_CHAN_ALLOCED    (1)
#define LIB_EMT_EDMA3_CHAN_NOTALLOCED (0)

/* LibArch channel info structure to store information of an EDMA3 DRV channel */
typedef struct lib_emt_channel_info_s {
    uint16_t flag;   /* to indicate if an EDMA3 DRV channel has been associated
                        with this LibArch channel */
    uint32_t chId;   /* EDMA3 DRV channel id, returned from EDMA3 LLD */
    uint32_t tcc;    /* DMA3 DRV channel number on which the transfer completion/error 
                        interrupt is generated, returned from EDMA3 LLD */
} lib_emt_channel_info_t;

/* LibArch external memory transfer structure to track EDMA3 resources used by LibArch */
typedef struct lib_emt_inst_s {
    EDMA3_DRV_Handle hEdma;  /* handle of EDMA3 LLD */
    /* array of LibArch channel information structures, each associated with an 
       EDMA3 DRV channel */
    lib_emt_channel_info_t chanInfo[LIB_EMT_MAX_PAR_CHANNELS];
} lib_emt_inst_t;

/* Initialize the structure that tracks EDMA3 resources  */
lib_emt_inst_t lib_emt_inst = 
{
    NULL,          /* EDMA3 DRV handle initialized to NULL */
    /* channel info structures initialized to not associated with EDMA3 channel */
    {{LIB_EMT_EDMA3_CHAN_NOTALLOCED,0,0},
     {LIB_EMT_EDMA3_CHAN_NOTALLOCED,0,0},
     {LIB_EMT_EDMA3_CHAN_NOTALLOCED,0,0},
     {LIB_EMT_EDMA3_CHAN_NOTALLOCED,0,0},
     {LIB_EMT_EDMA3_CHAN_NOTALLOCED,0,0},
     {LIB_EMT_EDMA3_CHAN_NOTALLOCED,0,0},
     {LIB_EMT_EDMA3_CHAN_NOTALLOCED,0,0},
     {LIB_EMT_EDMA3_CHAN_NOTALLOCED,0,0}}
};

/* Macros to fill the OPT Field of EDMA3 configuration */
#define OPT_SYNCDIM_SHIFT                   (0x00000002u)
#define OPT_TCC_MASK                        (0x0003F000u)
#define OPT_TCC_SHIFT                       (0x0000000Cu)
#define OPT_ITCINTEN_SHIFT                  (0x00000015u)
#define OPT_TCINTEN_SHIFT                   (0x00000014u)
#define OPT_TCCMOD_SHIFT                    (0x0000000Bu)

/**
 * @brief Refer to lib_datatrans.h for detailed documentation.
 */
int32_t lib_emt_init(lib_emt_Config_t *cfg)
{
    /* check input parameters */
    if(cfg == NULL) {
        return (LIB_EMT_ERROR_INVARG);
    }
    else if(cfg->hEdma == NULL) {
        return (LIB_EMT_ERROR_INVCFG);
    }
    else {
        lib_emt_inst.hEdma = cfg->hEdma; 
        return (LIB_EMT_SUCCESS);
    }
} /* lib_emt_init */

/**
 * @brief Refer to lib_datatrans.h for detailed documentation. 
 * @remark Linked transfer not yet implemented for K2G. 
 */
lib_emt_Handle lib_emt_alloc(int32_t max_linked_transfers)
{
    EDMA3_DRV_Result result;
    uint32_t chId = 0;
    uint32_t tcc = 0;
    int i, chan_id;

    /* Scan all channel info structures until finding one not associated to
       any EDMA3 DRV channel */
    for(i=0; i<LIB_EMT_MAX_PAR_CHANNELS; i++)
    {
        if(lib_emt_inst.chanInfo[i].flag == LIB_EMT_EDMA3_CHAN_NOTALLOCED) {
            chan_id = i;
            break;
        }
    }
    
    /* Return NULL if all LibArch channels are used. */
    if(i == LIB_EMT_MAX_PAR_CHANNELS) {
        return (NULL);
    }
    
    /* Obtain an EDMA3 channel if ther is available LibArch channel */
    tcc  = EDMA3_DRV_TCC_ANY;
    chId = EDMA3_DRV_DMA_CHANNEL_ANY;

    /* Request any EDMA3 channel and any TCC */
    result = EDMA3_DRV_requestChannel(lib_emt_inst.hEdma, &chId, &tcc, 
                                      (EDMA3_RM_EventQueue)0, NULL, NULL);

    if(result != EDMA3_DRV_SOK) {
        return (NULL);  /* no available EDMA3 DRV channel */
    }
    else {
        /* associate this EDMA3 DRV channel to LibArch channel */
        lib_emt_inst.chanInfo[chan_id].tcc  = tcc;
        lib_emt_inst.chanInfo[chan_id].chId = chId;
        lib_emt_inst.chanInfo[chan_id].flag = LIB_EMT_EDMA3_CHAN_ALLOCED;
        
        /* return LibArch channel handle */
        return ((lib_emt_Handle)&lib_emt_inst.chanInfo[chan_id]);
    }
} /* lib_emt_alloc */

/**
 * @brief Refer to lib_datatrans.h for detailed documentation.
 */
int32_t lib_emt_copy1D1D(lib_emt_Handle h, void *restrict src,
                         void *restrict dst, int32_t num_bytes)
{
    EDMA3_DRV_Result result;
    EDMA3_DRV_PaRAMRegs paramSet = {0,       /* opt to be set */
                                    0,       /* srcAddr to be set */
                                    0,       /* aCnt to be set */
                                    1,       /* bCnt = 1 */
                                    0,       /* destAddr to be set */
                                    0,       /* srcBIdx  = 0 */
                                    0,       /* destBIdx = 0 */
                                    0xFFFFu, /* linkAddr, no linking */ 
                                    0,       /* bCntReload = 0 */ 
                                    0,       /* srcCIdx = 0 */
                                    0,       /* dstCIdx = 0 */
                                    1,       /* cCnt = 1 */
                                    0};      /* reserved */
    lib_emt_channel_info_t *chan_info = (lib_emt_channel_info_t *)h;
    
    /* Fill the PaRAM Set with transfer specific information */
    paramSet.srcAddr  = (uint32_t)(src);
    paramSet.destAddr = (uint32_t)(dst);

    /**
    * Be Careful !!!
    * Valid values for ACNT/BCNT/CCNT are between 0 and 65535.
    * ACNT/BCNT/CCNT must be greater than or equal to 1.
    * Maximum number of bytes in an array (ACNT) is 65535 bytes
    * Maximum number of arrays in a frame (BCNT) is 65535
    * Maximum number of frames in a block (CCNT) is 65535
    */
    paramSet.aCnt = num_bytes;

    /* Program the OPT field: A sync, TCC */
    paramSet.opt = ((chan_info->tcc << OPT_TCC_SHIFT) & OPT_TCC_MASK);

    /* Enable final transfer completion interrupt */
    paramSet.opt |= (1 << OPT_TCINTEN_SHIFT);

    /* Now, write the PaRAM Set. */
    result = EDMA3_DRV_setPaRAM(lib_emt_inst.hEdma, chan_info->chId, &paramSet);

    if (result != EDMA3_DRV_SOK) {
        return (LIB_EMT_ERROR_INVCFG);
    }

    /* Now enable the transfer. */
    result = EDMA3_DRV_enableTransfer(lib_emt_inst.hEdma, chan_info->chId, EDMA3_DRV_TRIG_MODE_MANUAL);
    if (result != EDMA3_DRV_SOK) {
        return (LIB_EMT_ERROR_XFER);
    }

    return (LIB_EMT_SUCCESS);
    
} /* lib_emt_copy1D1D */

/**
 * @brief Refer to lib_datatrans.h for detailed documentation.
 */
int32_t lib_emt_copy2D2D(lib_emt_Handle h, void *restrict src, void *restrict dst, 
                         int32_t num_bytes_per_line, int32_t num_lines, 
                         int32_t src_pitch, int32_t dst_pitch)
{
    EDMA3_DRV_Result result;
    EDMA3_DRV_PaRAMRegs paramSet = {0,       /* opt to be set */
                                    0,       /* srcAddr to be set */
                                    0,       /* aCnt to be set */
                                    0,       /* bCnt to be set */
                                    0,       /* destAddr to be set */
                                    0,       /* srcBIdx  to be set */
                                    0,       /* destBIdx to be set */
                                    0xFFFFu, /* linkAddr, no linking */ 
                                    0,       /* bCntReload = 0 */ 
                                    0,       /* srcCIdx = 0 */
                                    0,       /* dstCIdx = 0 */
                                    1,       /* cCnt = 1 */
                                    0};      /* reserved */
    lib_emt_channel_info_t *chan_info = (lib_emt_channel_info_t *)h;

    /* Fill the PaRAM Set with transfer specific information */
    paramSet.srcAddr  = (uint32_t)(src);
    paramSet.destAddr = (uint32_t)(dst);

    /**
    * Be Careful !!!
    * Valid values for SRCBIDX/DSTBIDX are between -32768 and 32767
    * Valid values for SRCCIDX/DSTCIDX are between -32768 and 32767
    */
    paramSet.srcBIdx  = src_pitch;
    paramSet.destBIdx = src_pitch;

    /**
    * Be Careful !!!
    * ACNT/BCNT/CCNT must be greater than or equal to 1.
    * Maximum number of bytes in an array (ACNT) is 65535 bytes
    * Maximum number of arrays in a frame (BCNT) is 65535
    * Maximum number of frames in a block (CCNT) is 65535
    */
    paramSet.aCnt = num_bytes_per_line;
    paramSet.bCnt = num_lines;

    /* Src & Dest are in INCR modes, AB sync,  */
    paramSet.opt = (1 << OPT_SYNCDIM_SHIFT);

    /* Program the TCC */
    paramSet.opt |= ((chan_info->tcc << OPT_TCC_SHIFT) & OPT_TCC_MASK);

    /* Enable final transfer completion interrupt */
    paramSet.opt |= (1 << OPT_TCINTEN_SHIFT);

    /* Now, write the PaRAM Set. */
    result = EDMA3_DRV_setPaRAM(lib_emt_inst.hEdma, chan_info->chId, &paramSet);

    if (result != EDMA3_DRV_SOK) {
        return (LIB_EMT_ERROR_INVCFG);
    }

    /* Now enable the transfer. */
    result = EDMA3_DRV_enableTransfer(lib_emt_inst.hEdma, chan_info->chId, EDMA3_DRV_TRIG_MODE_MANUAL);
    if (result != EDMA3_DRV_SOK) {
        return (LIB_EMT_ERROR_XFER);
    }

    return (LIB_EMT_SUCCESS);
    
} /* lib_emt_copy2D2D */

/**
 * @brief Refer to lib_datatrans.h for detailed documentation.
 */
int32_t lib_emt_wait(lib_emt_Handle h)
{
    EDMA3_DRV_Result result;
    lib_emt_channel_info_t *chan_info = (lib_emt_channel_info_t *)h;

    result = EDMA3_DRV_waitAndClearTcc (lib_emt_inst.hEdma, chan_info->tcc);

    if (result != EDMA3_DRV_SOK) {
        printf("lib_emt_wait: EDMA3_DRV_waitAndClearTcc() FAILED, " \
               "error code: %d\r\n", (int)result);
    }

    return result;
} /* lib_emt_wait */

/**
 * @brief Refer to lib_datatrans.h for detailed documentation.
 */
int32_t lib_emt_free(lib_emt_Handle h)
{
    EDMA3_DRV_Result result;
    lib_emt_channel_info_t *chan_info = (lib_emt_channel_info_t *)h;

    result = EDMA3_DRV_freeChannel (lib_emt_inst.hEdma, chan_info->chId);

    if (result != EDMA3_DRV_SOK) {
        return LIB_EMT_ERROR_FREE;
    }
    else {
        chan_info->flag = LIB_EMT_EDMA3_CHAN_NOTALLOCED;
        return LIB_EMT_SUCCESS;
    }
} /* lib_emt_free */


#endif /* end of #ifdef SOC_K2G  */


/* nothing past this line */

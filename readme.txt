Library Architecture and Framework (LibArch) build instructions. 

1.--------- Set environment variables ---------

Following environment vaialbes must be set in order to build LibArch (version numbers are used as examples):

export CGTROOT="<TI_CGT_INSTALLATION_ROOT>/cgt-c6x"
export PDK_DIR="<COMPONENTS_INSTALLATION_ROOT>/pdk_c667x_2_0_1"
export FC_DIR="<COMPONENTS_INSTALLATION_ROOT>/framework_components_3_40_01_04"
export XDAIS_DIR="<COMPONENTS_INSTALLATION_ROOT>/xdais_7_24_00_04"
export BIOS_DIR="<COMPONENTS_INSTALLATION_ROOT>/bios_6_45_01_29"
export OMP_DIR="<COMPONENTS_INSTALLATION_ROOT>/openmp_dsp_c667x_2_02_00_02"
export XDC_DIR="<COMPONENTS_INSTALLATION_ROOT>/xdctools_3_32_00_06_core"
export IPC_DIR="<COMPONENTS_INSTALLATION_ROOT>/ipc_3_42_00_02"
export EDMA3_DIR="<COMPONENTS_INSTALLATION_ROOT>/edma3_lld_02_12_01_23"
export PATH=<TI_CGT_INSTALLATION_ROOT>/cgt-c6x/bin:$PATH

2.--------- Build LibArch RTSC package ---------

From the root folder of LibArch, the build command is:
make -f build/Makefile TARGET=<target_name> LIBOS=<os_name>

where:
   1) <target_name> must be one of SOC_K2H, SOC_C6678, SOC_AM572x, or SOC_K2G
   2) <os_name> must be one of LIB_OPENCL or LIB_RTOS

Examples:
1) to build LibArch for K2H in OpenCL environment, type:
make -f build/Makefile TARGET=SOC_K2H LIBOS=LIB_OPENCL

2) to build LibArch for C6678 in RTOS environment, type:
make -f build/Makefile TARGET=SOC_C6678 LIBOS=LIB_RTOS

The generated LibArch RTSC package is a .zip file in the exports folder. 

3.--------- Build LibArch examples for K2H, C6678, AM572x ---------
1) Install the LibArch package generated in previous step by unzipping the package file in folder ./exports:
   - libarch_<version>.zip
   
2) Add one more environment variable to point to LibArch's path:
export LIBARCH_DIR="<LibArch installation folder>"

3) Go to examples folder and use build command:
make TARGET=<target_name> LIBOS=<os_name>

where: 
   1) <target_name> must be one of SOC_K2H, SOC_C6678, or SOC_AM572x
   2) <os_name> must be one of LIB_OPENCL or LIB_RTOS

4.--------- Build LibArch DSP-only examples for K2G ---------
1) Import CCS project from: examples/dsponly/k2g/ccs_project
2) Make sure all RTSC products specified in project Properties->CCS General->RTSC->Products and Repositories
   are installed properly. 
3) Build the project

5. --------- Build LibArch source code ---------
1) Go to <LibArch installation folder>/packages/ti/libarch/src
2) make TARGET=<target_name> LIBOS=<os_name>, e.g. 
   - to build for K2G: make TARGET=SOC_K2G LIBOS=LIB_RTOS
   - to build for AM572x: make TARGET=SOC_AM572x LIBOS=LIB_OPENCL
3) The generated binary is ../lib/libarch.ae66

